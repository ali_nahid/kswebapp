package com.ks.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KSWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(KSWebApplication.class, args);
	}

}
