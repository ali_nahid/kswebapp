package com.ks.webapp.components.heavyliftings;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.ArrayBlockingQueue;

import com.ks.webapp.pojos.BigTask;
import com.ks.webapp.services.SendTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

@Component
public class BigTaskHandler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    private SendTask sendTask;

    @Async
    public ListenableFuture<String> handleTheBigOperation(String jsonUrl, BigTask theTask, ArrayBlockingQueue<String> tasks) {
        try {
            //  https://bitbucket.org/ali_nahid/datasource/raw/13491a8d7c041c86ccf227675cbec757f1e0f5df/smalldb.json;
            // "https://bitbucket.org/ali_nahid/datasource/raw/13491a8d7c041c86ccf227675cbec757f1e0f5df/db.json"
            sendTask.send(jsonUrl, theTask);
            tasks.remove(theTask.getTaskId());
        } catch (MalformedURLException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return new AsyncResult<String>(theTask.getTaskId());
    }
}