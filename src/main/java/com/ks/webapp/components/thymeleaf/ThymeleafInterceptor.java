package com.ks.webapp.components.thymeleaf;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class ThymeleafInterceptor extends HandlerInterceptorAdapter {
    private static final String DFAULT_LAYOUT="layouts/default";
    private static final String DEFAULT_VIEW_ATTRIBUTE_NAME = "view";

    private String layout = ThymeleafInterceptor.DFAULT_LAYOUT;
    private String viewAttribute = DEFAULT_VIEW_ATTRIBUTE_NAME;

    @Override
    public void postHandle(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, java.lang.Object handler, org.springframework.web.servlet.ModelAndView modelAndView) throws java.lang.Exception {
        if(modelAndView == null || !modelAndView.hasView()) {
            return;
        }

        String incomingViewName = modelAndView.getViewName();
        if(this.isRedirectOrForward(incomingViewName)) {
            return;
        }

        String layoutName = this.layout;

        HandlerMethod handlerMethod = (HandlerMethod)handler;
        Layout layout = handlerMethod.getMethodAnnotation(Layout.class);
        if(layout == null) {
            layout = handlerMethod.getBeanType().getAnnotation(Layout.class);
        }
        if(layout != null) {
            layoutName = layout.value();
        }
        
        modelAndView.setViewName(layoutName);
        modelAndView.addObject(this.viewAttribute, incomingViewName);
    }

    private boolean isRedirectOrForward(String viewName) {
        return viewName.toLowerCase().startsWith("redirect:") || viewName.toLowerCase().startsWith("forward:");
    }

}