package com.ks.webapp.controllers;

import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ks.webapp.components.heavyliftings.BigTaskHandler;
import com.ks.webapp.pojos.BigTask;
import com.ks.webapp.services.SendTask;
import com.ks.webapp.sessions.TasksTrackingService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/")
public class HomeController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SendTask sendTask;
    @Autowired
    private TasksTrackingService taskTrackingService;
    @Autowired
    private BigTaskHandler bigTaskHandler;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("home");
        mv.addObject("bigoperationresult", "this is home page");
        return mv;
    }

    @RequestMapping(params = { "sendtask" }, method = RequestMethod.POST)
    public String sendTaskSubmit(String taskPairs) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
            BigTask task = mapper.readValue(taskPairs, BigTask.class);
            this.sendTask.send(task);
        } catch (Exception e) {
            e.printStackTrace();
            ;
            this.logger.error(e.toString());
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/bigtask", method = RequestMethod.GET)
    public ModelAndView getBigTaskResult() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("home");        
        mv.addObject("bigoperationresult", "Please wait..In progress tasks: "+this.taskTrackingService.toString());
        return mv;
    }

    @RequestMapping(value = "/bigtask", params = { "bigtask" }, method = RequestMethod.POST)
    public ModelAndView sendBigTask(String jsonurl) throws InterruptedException, ExecutionException {
        BigTask bt = new BigTask();
        ListenableFuture<String> future = this.bigTaskHandler.handleTheBigOperation(jsonurl, bt, this.taskTrackingService.getTasks());
        future.addCallback(new ListenableFutureCallback<String>() {

            @Override
            public void onSuccess(String val) {
                final Logger logger0 = LoggerFactory.getLogger(HomeController.class);
                logger0.info("Task completed notification: "+val);
            }

            @Override
            public void onFailure(Throwable arg0) {
                final Logger logger0 = LoggerFactory.getLogger(HomeController.class);
                logger0.error(arg0.getMessage());
            }

        });
        ModelAndView mv = new ModelAndView();
        mv.setViewName("home");
        mv.addObject("bigoperationresult", "submitted taskId: "+bt.getTaskId() );
        this.taskTrackingService.addTask(bt.getTaskId());
        return mv;
    }
}