package com.ks.webapp.pojos;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BigTask implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4695715020926478523L;

    @JsonProperty("taskId")
    private String taskId;

    @JsonProperty("jdata")
    private List<JData> jData;

    public BigTask() {
        this.taskId = UUID.randomUUID().toString();
    }

    public String getTaskId() {
        return this.taskId;
    }

    public List<JData> getjData() {
        return jData;
    }

    public void setjData(List<JData> jData) {
        this.jData = jData;
    }
 
    
}