package com.ks.webapp.services;

import java.io.IOException;
import java.net.MalformedURLException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ks.webapp.pojos.BigTask;

public interface SendTask {
    void send(BigTask bigTask) throws JsonProcessingException;
    void send(String url, BigTask theTask) throws JsonProcessingException, MalformedURLException, IOException;
}