package com.ks.webapp.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ks.webapp.pojos.BigTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SendTaksImpl implements SendTask {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void send(BigTask bigTask) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(bigTask);
        logger.info("sending to rabbit");
        this.rabbitTemplate.convertAndSend(json);
    }

    @Override
    public void send(String url, BigTask theTask) throws JsonParseException, MalformedURLException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        URL urlObj = new URL("http://slowwly.robertomurray.co.uk/delay/10000/url/"+url);
        String json = this.URLReader(urlObj);
        //logger.info(json);
        BigTask bigTask = mapper.readValue(json, BigTask.class);
        theTask.setjData(bigTask.getjData());
        this.rabbitTemplate.convertAndSend(mapper.writeValueAsString(theTask));
    }
    

    private String URLReader(URL url) throws IOException {
        InputStream is = url.openStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        
        StringBuilder sb = new StringBuilder();

        String i;
        while( (i = br.readLine()) !=null) {
            sb.append(i);
        }
        br.close();
        return sb.toString();
    }
}