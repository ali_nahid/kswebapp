package com.ks.webapp.sessions;

import java.io.Serializable;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

@SessionScope
@Service
public class TasksTrackingServiceImpl implements TasksTrackingService, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2282835305256598837L;

    //private List<String> tasksSubmitted;

    private ArrayBlockingQueue<String> tasks;

    public TasksTrackingServiceImpl() {
        //this.tasksSubmitted = new ArrayList<String>();
        this.tasks = new ArrayBlockingQueue<String>(30);
    }

    public void addTask(String taskId) {
        //this.tasksSubmitted.add(taskId);
        try {
            this.tasks.offer(taskId, 10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {

        }
    }

    public String removeTask(String taskId) {
        //boolean b = this.tasksSubmitted.remove(taskId);
        try {
            return this.tasks.poll(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            
        }
        return null;
    }

    public boolean contains(String taskId) {
        return this.tasks.contains(taskId);
    }

    @Override
    public String toString() {
        String allTasks = "[";
        
        if(!this.tasks.isEmpty()) {
            for(Object s : this.tasks.toArray()) {
                allTasks += s+",";
            }
            allTasks = allTasks.substring(0, allTasks.length()-1);
        }
        allTasks +=  "]";

        return allTasks;
    }

    @Override
    public ArrayBlockingQueue<String> getTasks() {
        return tasks;
    }

    public void setTasks(ArrayBlockingQueue<String> tasks) {
        this.tasks = tasks;
    }

}