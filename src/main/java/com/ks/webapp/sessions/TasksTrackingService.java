package com.ks.webapp.sessions;

import java.util.concurrent.ArrayBlockingQueue;

public interface TasksTrackingService {
    public void addTask(String taskId);
    public String removeTask(String taskId);
    public ArrayBlockingQueue<String> getTasks();
}
